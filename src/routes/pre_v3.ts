import {Ctx} from "../BnS_API";
import {beautifyJSON, queryManager} from "../JS/_functions";
import json2csv from "json2csv";

export const v1 = (ctx: Ctx) => {
    const { database, server } = ctx
    const fields = ['Name','ID','Quantity','ItemPrice','LastUpdate'];

    server.get('/bns/v1/market/current/:region', async(req, res) => {
        let {limit, skip, region} = queryManager(req)

        let data = await database.db.collection("Market_Current_Top")
            .aggregate(
                [
                    {$match: { region: region } },
                    {$project : {
                            _id: 0,
                            ID : "$id",
                            Name: "$name",
                            ItemPrice:{ $trunc:{$divide: [{ $arrayElemAt: [ "$listings.price", 0 ] },{ $arrayElemAt: [ "$listings.count", 0 ] }]}} ,
                            Quantity: { $arrayElemAt: [ "$listings.count", 0 ] },
                            LastUpdate:"$ISO"
                        } }]
            ).skip(skip).limit(limit).toArray().catch((err) => {console.error(err);return []})

        let csv = json2csv({ data: data, fields: fields })
        res.setHeader('content-type', 'text/csv')

        return res.send(200, csv)
    })
}


export const v2 = (ctx: Ctx) => {
    const { database, server } = ctx

    server.get('/bns/v2/market/', async(req, res) => {
        let {limit, skip, beautify} = queryManager(req)

        let data = await database.db.collection("Item").aggregate([{$project: {_id: 0, Name: "$name", ID: "$id"}}]).skip(skip).limit(limit).toArray().catch((err) => {console.error(err);return []})

        return res.sendRaw(200, beautifyJSON(data, beautify), {'Content-Type': "application/json; charset=utf-8"})
    })

    server.get('/bns/v2/market/current/:region', async(req, res) => {
        let {limit, skip, beautify, region} = queryManager(req)

        let data = await database.db.collection("Market_Current_Top").aggregate(
            [
                { $match: { region: region } },
                { $project : {
                        _id: 0,
                        ID : "$id",
                        ItemPrice:{ $trunc:{$divide: [{ $arrayElemAt: [ "$listings.price", 0 ] },{ $arrayElemAt: [ "$listings.count", 0 ] }]}} ,
                        Quantity: { $arrayElemAt: [ "$listings.count", 0 ] },
                        ISO:1
                    } }]
        ).skip(skip).limit(limit).toArray().catch((err) => {console.error(err);return []})

        return res.sendRaw(200, beautifyJSON(data, beautify), {'Content-Type': "application/json; charset=utf-8"})
    })

    server.get('/bns/v2/market/history/:region/:id/', async(req, res) => {
        let {limit, skip, beautify, region, id} = queryManager(req)

        let data = database.db.collection("Market_History").aggregate(
            [
                { $match : { id:id , region: region} },
                { $project : {
                        _id: 0,
                        ID : "$id",
                        timeDate: "$ISO",
                        minPrice:{$divide: [ {$arrayElemAt: [ "$listings.price", 0 ]}, {$arrayElemAt: [ "$listings.count", 0 ]}]},
                        avgPrice:{ $trunc:{$divide: [ {$sum:"$listings.price"}, {$sum:"$listings.count"}]}} ,
                        maxPrice:{$divide: [ {$arrayElemAt: [ "$listings.price", -1 ]}, {$arrayElemAt: [ "$listings.count", -1 ]}]},
                        totalItems:{$sum:"$listings.count"},
                        totalListings: "$totalListings"

                    } }]
        ).skip(skip).limit(limit).toArray().catch((err) => {console.error(err);return []})

        return res.sendRaw(200, beautifyJSON(data, beautify), {'Content-Type': "application/json; charset=utf-8"})
    })
}