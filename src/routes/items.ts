import {queryManager, defaultPath, PathOptions, beautifyJSON} from "../JS/_functions"
import {Ctx} from "../BnS_API";
import {Equipment, Gems, Item} from "../config/db_entities";

export const equipment = (ctx: Ctx) => {
    const { database, server } = ctx

    server.get('/bns/v3/equipment', async (req, res) => {
        let {limit, skip, beautify, query, fields} = queryManager(req)

        let project = ["id", "name", "icon", "level", "type"]
        if (fields.length > 0){
            project = fields
        }

        let results = await database.get_items(Equipment, query, project, limit, skip, {id:1})
        return res.sendRaw(200, beautifyJSON(results,beautify),{'Content-Type': "application/json; charset=utf-8"})
    })

    server.get('/bns/v3/equipment/keys', async(req, res) => {
        let {beautify} = queryManager(req)
        await defaultPath("key", database, res, req, "Equipment", new PathOptions({beautify:beautify}))
    })


    server.get('/bns/v3/equipment/:sorting', async(req, res) => {
        let {limit, skip, beautify, query, sorting} = queryManager(req)

        let sort = {};
        if(sorting === "new"){
            sort = {firstAdded:-1}
        }
        if(sorting === "old"){
            sort = {firstAdded:1}
        }
        if(sorting === "count"){
            sort = {ISO:-1};
            query = {};
        }

        let results = await database.get_items(Equipment, query, undefined, limit, skip, sort)

        if(sorting === "count"){
            return res.sendRaw(200, beautifyJSON(count_general(results, "type"),beautify),{'Content-Type': "application/json; charset=utf-8"})
        } else {
            return res.sendRaw(200, beautifyJSON(results,beautify),{'Content-Type': "application/json; charset=utf-8"})
        }
    })
};


export const items = (ctx: Ctx) => {
    const { database, server } = ctx

    // Returns Name, ID and the img link
    server.get('/bns/v3/item', async (req, res) => {
        let {limit, skip, beautify, query, fields} = queryManager(req)

        let project = []
        if (fields.length > 0){
            project = fields
        }

        let results = await database.get_items(Item, query, project, limit, skip, {id:1})
        return res.sendRaw(200, beautifyJSON(results,beautify),{'Content-Type': "application/json; charset=utf-8"})
    })

    server.get('/bns/v3/item/keys', async(req, res) => {
        let {beautify} = queryManager(req)
        await defaultPath("key", database, res, req, "Item", new PathOptions({beautify:beautify}))
    })

    server.get('/bns/v3/item/:sorting', async(req, res) => {
        let {limit, skip, beautify, query, sorting} = queryManager(req)

        let sort = {};
        if(sorting === "new"){
            sort = {firstAdded:-1}
        }
        if(sorting === "old"){
            sort = {firstAdded:1}
        }
        if(sorting === "count"){
            sort = {ISO:-1};
            query = {};
        }

        let results = await database.get_items(Item, query, undefined, limit, skip, sort)

        if(sorting === "count"){
            return res.sendRaw(200, beautifyJSON(count_general(results, "type"),beautify),{'Content-Type': "application/json; charset=utf-8"})
        } else {
            return res.sendRaw(200, beautifyJSON(results,beautify),{'Content-Type': "application/json; charset=utf-8"})
        }
    })
};


export const gems = (ctx: Ctx) => {
    const { database, server } = ctx

    // Returns Name, ID and the img link
    server.get('/bns/v3/gem', async (req, res) => {
        let {limit, skip, beautify, query, fields} = queryManager(req)

        let project = ["id", "name", "icon", "level", "category_minor_name"]
        if (fields.length > 0){
            project = fields
        }

        let results = await database.get_items(Gems, query, project, limit, skip, {id:1})
        return res.sendRaw(200, beautifyJSON(results,beautify),{'Content-Type': "application/json; charset=utf-8"})
    })

    server.get('/bns/v3/gem/keys', async(req, res) => {
        let {beautify} = queryManager(req)
        await defaultPath("key", database, res, req, "Gems", new PathOptions({beautify:beautify}))
    })

    server.get('/bns/v3/gem/:sorting', async(req, res) => {
        let {limit, skip, beautify, query, sorting} = queryManager(req)

        let sort = {};
        if(sorting === "new"){
            sort = {firstAdded:-1}
        }
        if(sorting === "old"){
            sort = {firstAdded:1}
        }
        if(sorting === "count"){
            sort = {ISO:-1};
            query = {};
        }

        let results = await database.get_items(Gems, query, undefined, limit, skip, sort)

        if(sorting === "count"){
            return res.sendRaw(200, beautifyJSON(count_general(results, "category_minor_name"),beautify),{'Content-Type': "application/json; charset=utf-8"})
        } else {
            return res.sendRaw(200, beautifyJSON(results,beautify),{'Content-Type': "application/json; charset=utf-8"})
        }
    })
};

function count_general(json, field?: string) {
    let data = {
        ISO: new Date().toISOString(),
        total: json.length
    }

    if(field){
        for (let i = 0; i < json.length; i++) {
            if (typeof data[json[i][field]] == "undefined") {
                data[json[i][field]] = 0
            }
            data[json[i][field]] += 1
        }
    }

    return data
}