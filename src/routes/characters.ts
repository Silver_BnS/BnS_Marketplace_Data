import {queryManager, beautifyJSON, get_character} from "../JS/_functions";
import {Ctx} from "../BnS_API";
import {Character} from "../config/db_entities";
/*
import {cleanQuery, characterCounts} from "../JS/_functions";
import {FilterQuery} from "mongodb";
 */

export const characters = (ctx: Ctx) => {
	const { database, server } = ctx

	// recreate v3

	/*
	server.get('/bns/v3/character/rankings/:orderby', async (req, res) => {
		let {limit, skip, beautify} = queryManager(req)
        if(limit >100){limit =100}

		let query;
		if(typeof req.query.playerClass !== "undefined"){
			let playerClassSplit = req.query.playerClass.split(",");
			if(playerClassSplit.length > 1){
				let classArray = [];
				for(let i=0;i<playerClassSplit.length; i++){
					let search: FilterQuery<Character> = { "base_info.class_name" : playerClassSplit[i] };
					classArray.push(search)
				}
				delete req.query.playerClass;
				query = {$and:[{ $or: classArray },cleanQuery(req.query)]}
			}else{
				query = cleanQuery(req.query) || {};
			}
		}else{
			query = cleanQuery(req.query) || {};
		}

		let ordering = {
			[req.params.orderby.toString()]: -1
		};

		let characters = await database.get_items(Character, query, ["_id"] , limit, skip, ordering)
		return res.sendRaw(200, beautifyJSON(characters,beautify),{'Content-Type': "application/json; charset=utf-8"})
    })
	
	server.get('/bns/v3/character/:sorting', async  (req, res) => {
		let {limit, skip, beautify, sorting, livecount, query} = queryManager(req)

		if(sorting === "eu" || sorting === "na"){
			query["region"] = sorting
		}

		// extra processing for these
		if(sorting === "count" && livecount === "true"){
			let characters = await database.get_items(Character, query, ["id"] , limit, skip)
			let sending = characterCounts(characters);
			return res.sendRaw(200, beautifyJSON(sending,beautify),{'Content-Type': "application/json; charset=utf-8"})
		}
		if(sorting === "all" || sorting === "eu" || sorting === "na"){
			let characters = await database.get_items(Character, query, ["name"] , limit, skip)
			let sending = characters.map(character => character.name).sort()
			return res.sendRaw(200, beautifyJSON(sending,beautify),{'Content-Type': "application/json; charset=utf-8"})
		}

		let characters = await database.get_items(Character, query, [] , limit, skip)
		return res.sendRaw(200, beautifyJSON(characters,beautify),{'Content-Type': "application/json; charset=utf-8"})
    });
	*/

	server.get('/bns/v4/character/:region/:name', async(req, res) => {
		let {region, name, beautify} = queryManager(req)

		if (region === "" || name === "") {
			return res.sendRaw(200, beautifyJSON({error: `${name} on ${region} not found`}, beautify), {'Content-Type': "application/json; charset=utf-8"});
		}

		let exists = await database.get_items(Character, {name: name, region: region}, []);
		// return this if it exists
		if (exists.length > 0 && exists[0].active) {
			return res.sendRaw(200, beautifyJSON(exists[0], beautify), {'Content-Type': "application/json; charset=utf-8"});
		}

		let character = await get_character(database, name, region, true);

		if (typeof character === "undefined" ) {
			if (exists.length > 0){
				// send the last copy of the data
				return res.sendRaw(200, beautifyJSON(exists[0], beautify), {'Content-Type': "application/json; charset=utf-8"});
			}else{
				return res.sendRaw(200, beautifyJSON({error: `${name} on ${region} not found`}, beautify), {'Content-Type': "application/json; charset=utf-8"});
			}
		}

		// Character is complete, send to user
		res.sendRaw(200, beautifyJSON(character, beautify), {'Content-Type': "application/json; charset=utf-8"});
	})
}