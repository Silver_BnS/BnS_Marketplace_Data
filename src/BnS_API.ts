import { name,version, port, env, db } from "./config/config"
import restify from "restify"
import { DB_Manager } from "./config/db"
import { routes } from "./config/routeController"
import { db_entities } from "./config/db_entities";

let server = restify.createServer({ name: name, version: version })
server.use(restify.plugins.bodyParser({ mapParams: true }))
server.use(restify.plugins.acceptParser(server.acceptable))
server.use(restify.plugins.queryParser({ mapParams: true }))
server.use(restify.plugins.fullResponse())

export interface Ctx {
  database: DB_Manager;
  server:  restify.Server;
}

server.listen(port, async() => {
  let database = new DB_Manager()
  await database.lift(db.uri, db.database, db_entities)

  routes({ database, server })

  // report success
  console.log(`[info]: ${name} ${version} ready to accept connections on port ${port} in ${env} environment.`)
})