import {v1, v2} from "../routes/pre_v3"
import {characters} from "../routes/characters"
import {equipment, items, gems} from "../routes/items"
import {market} from "../routes/market"
import {status} from "../routes/status"
import {Ctx} from "../BnS_API";

export const routes = (ctx: Ctx) => {
	// items subpage
	equipment(ctx)
	items(ctx)
	gems(ctx)

	characters(ctx)
	market(ctx)

	// counters

	// pre v2 emulators
	v2(ctx)
	v1(ctx)

	status(ctx)
}