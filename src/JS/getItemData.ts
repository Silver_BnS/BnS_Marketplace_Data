import axios from 'axios'
import {DB_Manager} from "../config/db"
import {db, gpvlu} from '../config/config'
import {Item} from "../config/db_entities";

const controller = async() =>{
	let database = new DB_Manager()
	await database.lift(db.uri, db.database)

	// new items dont have the img field
	let newItems = await database.get_items(Item, { img:null, id:{$gt:0}}, ["id"])

	// if there are no new items end it ehre
	if(newItems.length ===0){await database.down(); return }

	// get an array of ID's
	let ids = newItems.map(item => item.id)

	// get teh itemData for these items

	// get the details
	let details = await getDetails(ids)

	await database.update_bulk(Item, details)

	await database.down();
}

interface Response {
	id: string,
	name: string,
	grade: string,
	imageUrl: string,
	price: string,
	baseFee: string,
	level: string,
	job: string,
	job1: string,
	job2: string,
	sex: string,
	race: string,
	guideUnitPrice: string
}

async function getDetails (ids:Array<number>):Promise<Partial<Item>[]>{
	let requests = ids.map(async (id) =>{return await axios({
			method: 'GET',
			url:`http://eu-bnsmarket.ncsoft.com/bns/seller/item.web?item=${id}`,
			headers: { 'User-Agent': 'BnsIngameBrowser','Cookie': gpvlu,}
		}).then(response => response.data).catch(err => {console.error(err); return {}});
	})

	let detailsRaw:Response[] = await Promise.all(requests).catch(err => {console.error(err); return []});

	return detailsRaw
		// remove empty objects
		.filter(value => Object.keys(value).length !== 0)
		// convert to final form
		.map(convertDetails)

}

const convertDetails = (item: Response):Partial<Item>  =>{
	if(typeof item.id === "undefined"){
		return {id: 0}
	}

	let tmp:Partial<Item> = {
		id: parseInt(item.id, 10),
		characterLevel: parseInt(item.level, 10),
		guideUnitPrice: parseInt(item.guideUnitPrice, 10),
		img: item.imageUrl,
		itemTaxRate: parseInt(item.baseFee, 10) / 10,
		marketable: false,
		merchantValue: parseInt(item.price, 10),
		name: item.name,
		race: item.race,
		rank: parseInt(item.grade, 10),
		updated: new Date().toISOString(),
		sex: item.sex,
		class: []
	}

	if(item.job !== ""){
		tmp.class.push(item.job)
	}
	if(item.job1 !== ""){
		tmp.class.push(item.job1)
	}
	if(item.job2 !== ""){
		tmp.class.push(item.job2)
	}

	// clean it up
	for (const [key, value] of Object.entries(tmp)) {
		if(typeof value === "undefined"){
			delete tmp[key]
		}
		if(value === ""){
			delete tmp[key]
		}
	}

	return tmp;
}

controller().then(() => console.log("Complete")).catch((err) => console.error(err));