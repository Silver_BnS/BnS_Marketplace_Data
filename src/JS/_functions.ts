import { format } from 'fast-csv'
import JsonStreamStringify from "json-stream-stringify"
import {DB_Manager} from "../config/db";
import axios from "axios";
import {
    Character,
    Character_Equipment,
    Equipment,
    Gems,
    Result_Abilities,
    Result_Character,
    Result_Character_Alt,
    Result_Equipment
} from "../config/db_entities";

export const queryManager = (req)=> {
    // set defaults
    let limit = 20000
    let skip = 0
    let beautify = "human"
    let region = "eu"
    let id = 0
    let speed = "slow"
    let name = ""
    let site = "misc";
    let sorting = "count";
    let livecount = "false";
    let clan = "";
    let current = "current"
    let fields = []
    let filter = req.query.filter

    // if the fields exists process it
    if (req.query.limit) {limit = parseInt(req.query.limit, 10) || limit}
    if (req.query.skip) {skip = parseInt(req.query.skip, 10) || skip}
    if (req.query.beautify) {beautify = req.query.beautify || beautify}
    if (req.params.region) {region = req.params.region.toString().toLowerCase() || region}
    if (req.params.id) {id = req.params.id - 0 || id}
    if (req.params.speed) {speed = req.params.speed.toString().toLowerCase() || speed}
    if (req.params.name) {name = req.params.name.toString() || name}
    if (req.query.site) {site = req.query.site || site}
    if (req.params.sorting) {sorting = req.params.sorting.toString().toLowerCase() || sorting}
    if (req.query.livecount) {livecount = req.query.livecount.toString().toLowerCase() || livecount}
    if (req.params.clan) {clan = req.params.clan.toString() || clan}
    if (req.params.current) {current = capitalize(req.params.current.toString().toLowerCase()) || current}
    if (req.query.fields) {fields = req.query.fields.split(",") || fields}

    let query = cleanQuery(req.query) || {}

    return {limit, skip, beautify, region, id, speed, name, site, sorting, livecount, clan, query, current, fields, filter}
}

export function cleanBody(string){
    if(typeof string !== "string"){return string}

    string = string.replace(/&quot;/g,'"'); 						// cleans quotations
    string = string.replace(/&#xAE08;/g,""); 					// Cleans KR Gold
    string = string.replace(/&#xC740;/g,""); 					// Cleans KR Silver
    string = string.replace(/&#xB3D9;/g,""); 					// Cleans KR Bronze
    string = string.replace(/금/g,""); 							// Cleans KR Gold2
    string = string.replace(/은/g,""); 							// Cleans KR Silver2
    string = string.replace(/동/g,""); 							// Cleans KR Bronze2
    string = string.replace(/동/g,""); 							// Cleans KR Bronze2
    string = string.replace("<!-- (s) 비무 정보 ncw only--",""); 	// makes hidden div's visible
    string = string.replace("(e) 비무 정보 -->","") ;				// makes hidden div's visible
    string = string.replace(/ · /g,". Trigger:");				// Strange characters, no idea what they are

    return string
}

export function cleanQuery(query){
    let keys = Object.keys(query);
    for (let i=0; i < keys.length;i++){
        if(!isNaN(parseInt(query[keys[i]],10))){
            query[keys[i]] = parseInt(query[keys[i]],10)
        }
        if(query[keys[i]] === "true"){
            query[keys[i]] = true
        }
        if(query[keys[i]] === "false"){
            query[keys[i]] = false
        }
    }
    delete query.limit;
    delete query.skip;
    delete query.beautify;
    delete query.livecount;
    delete query.site;
    delete query.fields;
    return query
}

export class PathOptions  {
    public query?:object
    public project?:object
    public sort?:object
    public limit?:number
    public beautify?: string
    public transform?: any
    public headers?:Array<string> | boolean
    public skip?:number

    constructor(options?: PathOptions) {
        // set defaults for these
        if(options.query){this.query = options.query}else{this.query = {}}
        if(options.project){this.project = options.project}else{this.project = {_id:0}}
        if(options.sort){this.sort = options.sort}else{this.sort = {id:1}}
        if(options.limit){this.limit = options.limit}else{this.limit = 0}
        if(options.beautify){this.beautify = options.beautify}else{this.beautify = "human"}
        if(options.headers){this.headers = options.headers}else{this.headers = true}
        if(options.skip){this.skip = options.skip}else{this.skip = 0}

        // this is a function
        if(options.transform){this.transform = options.transform}
    }
}
export async function defaultPath(modifier, database: DB_Manager, res, req, collection:string, options:PathOptions){

    switch (modifier) {
        case 'key': {
            await defaultPathKey (database, res, collection, options)
            break
        }
        case 'csv': {
            await defaultPathCsv(database, res, collection, options, req.url)
            break
        }
        default: {
            await defaultPathJson(database, res, collection, options)
        }
    }
}


async function defaultPathCsv(database: DB_Manager, res, collection, options:PathOptions, url){
    const cursor = database.db.collection(collection).find(options.query).sort(options.sort).skip(options.skip).limit(options.limit)
    const filename = `api.silveress.ie${url.split("?")[0].replace(/\//gi, '_').replace("_csv", '.csv')}`

    // Set approrpiate download headers
    res.setHeader('Content-disposition', `attachment; filename=${filename}`);
    res.writeHead(200, { 'Content-Type': 'text/csv' });

    // Flush the headers before we start pushing the CSV content
    res.flushHeaders();

    // Pipe/stream the query result to the response via the CSV transformer stream
    let csvOptions = {
        headers: options.headers
    }

    if (options.transform) { csvOptions["transform"] = options.transform }

    cursor.stream()
        .pipe(format(csvOptions))
        .pipe(res)
}

async function defaultPathJson(database: DB_Manager, res, collection, options:PathOptions){
    const cursor = database.db.collection(collection).find(options.query).project(options.project).sort(options.sort).skip(options.skip).limit(options.limit).stream({transform:options.transform});
    // Set approrpiate download headers
    res.setHeader('Content-disposition', 'application/json; charset=utf-8');
    res.writeHead(200, { 'Content-Type': 'application/json; charset=utf-8' });
    res.flushHeaders();

    let spaces = options.beautify === "human" ? 2:0

    new JsonStreamStringify(cursor,undefined,spaces).pipe(res);
}


async function defaultPathKey (database: DB_Manager,res,collection,options:PathOptions){
    const keysOutput = await database.db.collection(collection).find({}).sort(options.sort).limit(1000).toArray()

    let result
    if(typeof options.transform === "undefined"){
        result = defaultKeys(keysOutput)
    }else{
        result = options.transform(keysOutput)
    }

    let spaces = options.beautify === "human" ? 2:0
    res.sendRaw(200, JSON.stringify(result, null, spaces), { 'Content-Type': 'application/json; charset=utf-8' })
}

const defaultKeys = (keysOutput):Array<any> =>{
    let keys = {}
    for (let i = 0; i < keysOutput.length; i++) {
        keys =  Object.assign({},keys,keysOutput[i]);
    }
    delete keys["_id"]
    return Object.keys(keys)
}

export function filterSorter (filterQuery) {
    let filter = {}
    if(typeof filterQuery === "undefined"){return filter}
    if (filterQuery.length >= 3 && filterQuery.indexOf(':') !== -1) {
        let splitFilter = filterQuery.split(',')
        let fieldObject = {}
        if (typeof splitFilter === 'object') {
            for (let i = 0; i < splitFilter.length; i++) {
                if (splitFilter[i].length >= 3) {
                    let field = splitFilter[i]
                    let c = field.split(':')
                    let searchTerm = c[1]
                    let tmp = {}
                    // boolean sorting
                    if (searchTerm === 'TRUE' || searchTerm === 'FALSE') {
                        if (searchTerm === 'TRUE') {
                            searchTerm = true
                        }
                        if (searchTerm === 'FALSE') {
                            searchTerm = false
                        }
                    }
                    // handing numerical operators
                    if (searchTerm === 'gt' || searchTerm === 'gte' || searchTerm === 'lt' || searchTerm === 'lte' || searchTerm === 'eq' || searchTerm === 'ne') {
                        tmp['$' + searchTerm] = c[2] - 0
                        searchTerm = tmp
                    }
                    // handing array search
                    if (searchTerm === 'in' || searchTerm === 'nin') {
                        tmp['$' + searchTerm] = c[2].split('.')
                        searchTerm = tmp
                    }
                    // handling regex search
                    if (searchTerm === 'cts') {
                        tmp['$regex'] = c[2]
                        tmp['$options'] = 'ix'
                        searchTerm = tmp
                    }

                    fieldObject[c[0]] = searchTerm
                }
            }
        }
        filter = fieldObject
    }
    return filter
}

function capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

export function beautifyJSON(body,flag) {
    if(flag ==="min" || flag ==="human"){
        if(flag ==="min"){
            return JSON.stringify(body);
        }
        if(flag ==="human"){
            return JSON.stringify(body, null, '\  ');
        }
    }else{
        return JSON.stringify(body, null, '\  ');
    }
}

export function characterCounts(json){
    let jsonEU = json.filter((item) => item.region === "eu");
    let jsonNA = json.filter((item) => item.region === "na");

    let data = {
        ISO: new Date().toISOString(),
        total: json.length,
        eu: jsonEU.length,
        na: jsonNA.length,
    };

    for (let i=0; i < jsonEU.length; i++){
        if(typeof data["eu " +jsonEU[i].playerClass] === "undefined"){
            data["eu " +jsonEU[i].playerClass] = 0;
        }
        data["eu " +jsonEU[i].playerClass]= data["eu " +jsonEU[i].playerClass]+1;
    }
    for (let i=0; i < jsonNA.length; i++){
        if(typeof data["na " +jsonNA[i].playerClass] === "undefined"){
            data["na " +jsonNA[i].playerClass] = 0;
        }
        data["na " +jsonNA[i].playerClass]= data["na " +jsonNA[i].playerClass]+1;
    }

    return data
}

export async function getURL<T>(url): Promise<T> {
    return await axios.get(encodeURI(url)).then(response => response.data).catch(() => {
        //console.error(url, err);
        return {}
    });
}

export async function get_character(database: DB_Manager, name: string, region: string, new_character: boolean, old_id: number = 0): Promise<Character | undefined> {
    /*
    (╯°□°）╯︵ ┻━┻
    EU: Both http and https work perfectly
        http://eu-bns.ncsoft.com/ingame/api/character/info.json?c=Silveress+Golden
        https://eu-bns.ncsoft.com/ingame/api/character/info.json?c=Silveress+Golden

    NA: Only http works
        http://na-bns.ncsoft.com/ingame/api/character/info.json?c=Silver
        https://na-bns.ncsoft.com/ingame/api/character/info.json?c=Silver
    */

    let http = "http"
    if(region === "eu"){
        http = "https"
    }

    let characterInfo = await getURL<Result_Character>(`${http}://${region}-bns.ncsoft.com/ingame/api/character/info.json?c=${name}`)

    if (typeof characterInfo.name === "undefined" || characterInfo.name === "" || characterInfo.name === null) {
        return undefined;
    }

    // character was deleted, update old one
    if(!new_character && old_id !==characterInfo.id){
        let tmp: Partial<Character> = {
            id: old_id,
            active: false,
        }
        await database.update_bulk(Character, [tmp], undefined, "id")
    }

    let equipmentInfo = await getURL<Result_Equipment>(`${http}://${region}-bns.ncsoft.com/ingame/api/character/equipments.json?c=${name}`);
    let abilitiesInfo = await getURL<Result_Abilities>(`${http}://${region}-bns.ncsoft.com/ingame/api/character/abilities.json?c=${name}`);
    let charNamesInfo = await getURL<Result_Character_Alt[]>(`${http}://${region}-bns.ncsoft.com/ingame/api/characters.json?guid=${characterInfo.account_id}`);

    //styleInfo: await getURL(`http://${region}-bns.ncsoft.com/ingame/api/training/characters/${name}/stat.json`),

    let equipment: Equipment[] = [];
    let gem_array: Gems[] = [];

    // process equipment
    let character_equipment: Character_Equipment = {}
    for (const [key, value] of Object.entries(equipmentInfo)) {
        let gems = [];
        let added_gems = value.detail.added_gems;
        if (added_gems && added_gems.length > 0) {
            for (let i = 0; i < added_gems.length; i++) {
                gems.push(added_gems[i].id)

                gem_array.push(added_gems[i]);
            }
        }

        let appearance = value.detail.item.id
        if (value.detail.appearance_item) {
            appearance = value.detail.appearance_item.id
        }

        character_equipment[key] = {
            id: value.detail.item.id,
            appearance,
            gems
        }

        equipment.push(value.detail.item)
    }

    let currentISO = new Date().toISOString();
    let character: Character = {
        id: characterInfo.id,
        account_id: characterInfo.account_id,
        name: characterInfo.name,
        active: true,
        characters: charNamesInfo.map(alt => {return {id: alt.id, region: region, name: alt.name}}),

        server_id: characterInfo.server_id,
        server_name: characterInfo.server_name,
        region: region,

        date_added: currentISO,
        date_updated_next: getNextUpdate(new_character, currentISO),
        date_updated: currentISO,

        guild_id: (characterInfo.guild) ? characterInfo.guild.guild_id : 0,
        guild_name: (characterInfo.guild) ? characterInfo.guild.guild_name : "",

        base_info: characterInfo,
        abilities: abilitiesInfo,

        equipment: character_equipment
    }

    // unsure what to do with these
    //character = setStyle(character, temp.styleInfo)

    // now to save the data properly
    await database.update_bulk(Character, [character])
    await database.update_bulk(Equipment, equipment)
    await database.update_bulk(Gems, gem_array)

    return character;
}

// updates are going to be spread across a week
export function getNextUpdate(newAccount: boolean, old: string) {
    let now = new Date();
    if (newAccount) {
        // when first added it randomises the time it is called for teh future
        now.setHours(Math.floor(Math.random() * ((24*7)-1)));
        now.setMinutes(Math.floor(Math.random() * 59));
    } else {
        now.setHours(new Date(old).getHours());
        now.setMinutes(new Date(old).getMinutes());
    }
    now.setHours(now.getHours() + (24*7));
    return now.toISOString();
}