import { DB_Manager} from "../config/db"
import {db} from "../config/config";
import {
    Item,
    Market_History_Compressed_Day,
    Market_History_Compressed_Hour
} from "../config/db_entities";

let now = new Date().toISOString()


controller().catch((err) => console.error(err));
async function controller() {
    console.time("market_compress")
    let database = new DB_Manager()
    await database.lift(db.uri, db.database)

    let idsRaw = await database.get_items(Item, {marketable: true}, ["id"]);
    let ids = idsRaw.map(item => item.id);
    //ids = [910109]

    await processRegion(database, ids);

    await database.down();
    console.timeEnd("market_compress")
}

const processRegion = async(database: DB_Manager,ids:Array<number>) =>{
    let week_tmp = new Date()
    week_tmp.setHours(week_tmp.getHours()-(24*7))
    week_tmp.setHours(1,0,0,0)
    let week = new Date(week_tmp).toISOString()

    //ids = [81360]
    let cleaned_compressed = [];
    for (const id of ids){
        // pull  a weeks worth od data from teh db,
        let hour = await database.get_items(Market_History_Compressed_Hour, {id:id});

        // compress
        let cleaned_compressed_raw: {[propName: string]: {[propName: string]: Market_History_Compressed_Day} } = {
            eu: {},
            na: {},
        }

        for(const entry of hour){
            let date = `${entry.ISO.split("T")[0]}T00:00:01.000Z`

            // only generate day's for more than a week old
            // days are deleted if they exceed this date so it should be a fairly seamless mix of teh two
            if(date < week){continue}
            if(typeof cleaned_compressed_raw[entry.region][date] === "undefined"){
                cleaned_compressed_raw[entry.region][date] = {
                    uuid: `${entry.id}_${entry.region}_${date}`,
                    id: entry.id,
                    region: entry.region,
                    ISO: date,
                    name: entry.name,
                    count:0,
                    price_min:entry.price_min,
                    price_avg:0,
                    price_max:entry.price_max,
                    price_total:0,
                    quantity_total: 0,
                    listings_total:0
                }
            }
            // incriment the count
            cleaned_compressed_raw[entry.region][date].count++

            // find the new max and min
            if(cleaned_compressed_raw[entry.region][date].price_min > entry.price_min){
                cleaned_compressed_raw[entry.region][date].price_min = entry.price_min
            }
            if(cleaned_compressed_raw[entry.region][date].price_max < entry.price_max){
                cleaned_compressed_raw[entry.region][date].price_max = entry.price_max
            }

            // add the totals
            cleaned_compressed_raw[entry.region][date].price_total += entry.price_total
            cleaned_compressed_raw[entry.region][date].quantity_total += entry.quantity_total
            cleaned_compressed_raw[entry.region][date].listings_total += entry.listings_total

            // sort out the averge
            cleaned_compressed_raw[entry.region][date].price_avg = Math.round(cleaned_compressed_raw[entry.region][date].price_total/ cleaned_compressed_raw[entry.region][date].quantity_total) || 0
        }

        cleaned_compressed.push(...Object.values(cleaned_compressed_raw["eu"]))
        cleaned_compressed.push(...Object.values(cleaned_compressed_raw["na"]))
    }
    // asdd it in one go
    await database.update_bulk(Market_History_Compressed_Day, cleaned_compressed, undefined, "uuid");
    // delete older data
    await database.delete_bulk(Market_History_Compressed_Hour, {delete_date:{$lt:now}});
}