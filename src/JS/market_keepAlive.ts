import axios from 'axios'
import cheerio from 'cheerio'
import {db, gpvlu} from '../config/config'
import {DB_Manager} from "../config/db"
import { cleanBody } from "./_functions";
import {Item} from "../config/db_entities";

let options = {
	url: `http://eu-bnsmarket.ncsoft.com/bns/bidder/search.web?ct=0-0&exact=&grade=0&level=0-0&prevq=&q=&sort=name-asc&stepper=""&type=SALE`,
	headers: {
		'User-Agent': 'BnsIngameBrowser',
		'Cookie': gpvlu
	}
}
axios(options)
	.then(async (response) => {
		let database = new DB_Manager()
		await database.lift(db.uri, db.database)

		let entry: Partial<Item> = {id: -1, updated: new Date().toISOString(), valid: true, name:"Silver's Flag"}

		if (response.status === 200){
			let body = cleanBody(response.data)
			let $ = cheerio.load(body);
			if ($('.pMarket.error section .message').text().trim() === "We apologize for the inconvenience. Please try again later.") {
				entry.valid = false
			}
		} else {
			entry.valid = false
		}

		await database.update_bulk(Item, [entry])
		await database.down();
	})
	.catch((err) => {console.error(err)})