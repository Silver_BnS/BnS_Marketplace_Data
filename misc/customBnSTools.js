function BnSToolsRankingPVE(character) {
    let max_element_rate    = Math.max(character.flameRate, character.frostRate, character.windRate, character.earthRate, character.shadowRate, character.lightningRate);
    let ap_elem             = (character.ap + character.ap_boss) / 2 * max_element_rate;
    let ap_elem_crit        = ap_elem * character.critDamageRate;
    let avg_damage          = character.critRate * ap_elem_crit + (1.0 - character.critRate) * ap_elem;
    let avg_damage_with_accu    = avg_damage * Math.min(character.accuracyRate - 0.24, 1.0);
    let bnstools_score_pve      = avg_damage_with_accu * Math.min(1.0 + character.piercingDefRate - 0.13, 1.0);

    return Math.round(bnstools_score_pve);
}

function BnSToolsRankingPVP(character) {
    let max_element_rate    = Math.max(character.flameRate, character.frostRate, character.windRate, character.earthRate, character.shadowRate, character.lightningRate);
    let ap_elem             = character.ap * max_element_rate;
    let ap_elem_crit        = ap_elem * character.critDamageRate;
    let fictive_crit_rate   = Math.max(character.critRate - 0.25, 0.0);
    let avg_damage_pvp      = fictive_crit_rate * ap_elem_crit + (1.0 - fictive_crit_rate) * ap_elem;
    let avg_damage_pvp_with_accu = avg_damage_pvp * Math.min(character.accuracyRate - 0.25, 1);
    let attack_scoring      = avg_damage_pvp_with_accu * Math.min(1 + character.piercingDefRate - 0.32, 1);

    let hp_def_coeff        = Math.max((character.hp - 100000) / 200.0, 0.0) * (1.0 + character.defenceDmgReduction);
    // let crit_def_coeff      = 1.0 + Math.max(0.5 - character.critDefRate, 0.0);
    let defense_scoring     = (hp_def_coeff * (1.0 + character.critDefRate) + 100.0 * character.blockRate + 100.0 * character.evasionRate);

    return Math.round(attack_scoring / 2.0 + defense_scoring);
}


module.exports.BnSToolsRankingPVE  = BnSToolsRankingPVE;
module.exports.BnSToolsRankingPVP  = BnSToolsRankingPVP;

let version_bns_tools_rankings = "1.0.1";
